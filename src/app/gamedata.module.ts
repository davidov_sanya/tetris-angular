export module Gamedata {

  export const FIELD_WIDTH = 10,
    FIELD_HEIGHT = 20,
    EMPTY_CELL = '#FFFFFF',
    SIZE_CELL = 26;

  const g: { is_rotate: boolean, data: number[][] } = {is_rotate: true, data: [[1, 1], [1, 0], [0, 0], [-1, 0]]},
    g2: { is_rotate: boolean, data: number[][] } = {is_rotate: true, data: [[1, -1], [1, 0], [0, 0], [-1, 0]]},
    z: { is_rotate: boolean, data: number[][] } = {is_rotate: true, data: [[-1, 0], [0, 0], [0, -1], [1, -1]]},
    z2: { is_rotate: boolean, data: number[][] } = {is_rotate: true, data: [[-1, -1], [0, -1], [0, 0], [1, 0]]},
    t: { is_rotate: boolean, data: number[][] } = {is_rotate: true, data: [[-1, 0], [0, 0], [1, 0], [0, 1]]},
    line: { is_rotate: boolean, data: number[][] } = {is_rotate: true, data: [[0, 2], [0, 1], [0, 0], [0, -1]]},
    square: { is_rotate: boolean, data: number[][] } = {is_rotate: false, data: [[1, 1], [1, 0], [0, 0], [0, 1]]};

  export let allFigure: object = [g, g2, z, z2, t, line, square],
    arrayColor = ['#DC143C', '#FF0000', '#8B0000', '#FF1493', '#FF4500', '#FFA500',
      '#8A2BE2', '#7FFF00', '#00FA9A', '#66CDAA', '#00FFFF', '#00008B'];


  export function getBufferFigure() {

    const figure: { color: string, is_rotate: boolean, data: number[][] } = this.allFigure[parseInt((Math.random() * 7).toString(), 10)],
      temp = parseInt((Math.random() * 4).toString(), 10);

    for (let i = 0; i <= temp; i++) {
      reverseFigure(figure);
    }
    figure.color = arrayColor[parseInt((Math.random() * arrayColor.length).toString(), 10)];
    return figure;
  }

  export function getNextFigure(figure) {
    return JSON.parse(JSON.stringify(figure));
  }

  export function getStartPosition(figure) {
    const minMax = searchMinMax(figure);
    return {
      x: 5,
      y: minMax.maxY
    };
  }

  export function checkNextLine(position, focusFigure, field) {
    const minMax = searchMinMax(focusFigure);
    if ((position.y - minMax.minY) >= 19) {
      return true;
    }
    for (let i = 0; i < focusFigure.data.length; i++) {
      if (field[position.y - focusFigure.data[i][1] + 1] [position.x + focusFigure.data[i][0]] !== EMPTY_CELL) {
        return true;
      }
    }
    return false;
  }


  export function checkReverse(position, focusFigure, field) {
    const minMax = searchMinMax(focusFigure);
    if (((position.x + minMax.minY) < 0) || ((position.x + minMax.maxY) > 9) || ((position.y + minMax.minX) < 0)) {
      return false;
    }
    for (let i = 0; i < focusFigure.data.length; i++) {
      if (field[position.y + focusFigure.data[i][0]][position.x + focusFigure.data[i][1]] !== EMPTY_CELL) {
        return false;
      }
    }
    return true;
  }

  export function checkMove(position, focusFigure, field, move) {
    const minMax = searchMinMax(focusFigure);
    if (((position.x + minMax.minX) === 0) && (move === -1)) {
      return false;
    }
    if (((position.x + minMax.maxX) === 9) && (move === 1)) {
      return false;
    }
    for (let i = 0; i < focusFigure.data.length; i++) {
      if (field[position.y - focusFigure.data[i][1]] [position.x + focusFigure.data[i][0] + move] !== EMPTY_CELL) {
        return false;
      }
    }
    return true;
  }

  export function checkFullLine(field, level, lines, score, speedLevel) {
    let flag = false;
    for (let i = field.length - 1; i >= 0; i--) {
      flag = true;
      for (let j = 0; j < field[i].length; j++) {
        if (field[i][j] === EMPTY_CELL) {
          flag = false;
          break;
        }
      }
      if (flag) {
        score += 100 + 10 * level;
        lines++;
        if (lines === 10) {
          level++;
          if (speedLevel < 301) {
            speedLevel -= 30;
          } else {
            speedLevel -= 100;
          }
          lines = 0;
        }
        for (let ii = i; ii > 0; ii--) {
          for (let jj = 0; jj < field[ii].length; jj++) {
            field[ii][jj] = field[ii - 1][jj];
          }
        }
        i++;
      }
    }
    return {level, lines, score, speedLevel};
  }

  export function fillField(position, focusFigure, field) {
    for (let i = 0; i < focusFigure.data.length; i++) {
      field[position.y - focusFigure.data[i][1]][position.x + focusFigure.data[i][0]] = focusFigure.color;
    }
  }

  export function reverseFigure(focusFigure) {
    if (focusFigure.is_rotate === false) {
      return;
    }
    let temp;
    for (let i = 0; i < focusFigure.data.length; i++) {
      temp = focusFigure.data[i][0];
      focusFigure.data[i][0] = focusFigure.data[i][1];
      focusFigure.data[i][1] = -temp;
    }
  }

  export function searchMinMax(focusFigure) {

    let minX = 0,
      minY = 0,
      maxX = 0,
      maxY = 0;

    for (let i = 0; i < focusFigure.data.length; i++) {
      if (minX > focusFigure.data[i][0]) {
        minX = focusFigure.data[i][0];
      }
      if (maxX < focusFigure.data[i][0]) {
        maxX = focusFigure.data[i][0];
      }
      if (minY > focusFigure.data[i][1]) {
        minY = focusFigure.data[i][1];
      }
      if (maxY < focusFigure.data[i][1]) {
        maxY = focusFigure.data[i][1];
      }
    }
    return {
      minX,
      maxX,
      minY,
      maxY,
    };
  }

}
