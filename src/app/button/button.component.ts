import {Component, OnInit, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @ViewChild('blurPause') blurPause: ElementRef;
  @ViewChild('blurRestart') blurRestart: ElementRef;
  @Output() onPause = new EventEmitter();
  @Output() onRestart = new EventEmitter();
  private focusPauseOff: HTMLButtonElement;
  private focusRestartOff: HTMLButtonElement;

  buttonPause() {
    this.focusPauseOff.blur();
    this.onPause.emit();
  }

  buttonRestart() {
    this.focusRestartOff.blur();
    this.onRestart.emit();
  }

  constructor() {
  }

  ngOnInit() {
    this.focusPauseOff = HTMLButtonElement = this.blurPause.nativeElement;
    this.focusRestartOff = HTMLButtonElement = this.blurRestart.nativeElement;
  }

}
