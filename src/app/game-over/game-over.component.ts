import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.css']
})
export class GameOverComponent implements OnInit {
  @ViewChild('blurRestart') blurRestart: ElementRef;
  @Output() onHideWindow = new EventEmitter();
  @Input() showLevel: number;
  @Input() showScore: number;

  buttonHideWindow() {
    this.onHideWindow.emit();
  }

  constructor() {
  }

  ngOnInit() {
  }

}
