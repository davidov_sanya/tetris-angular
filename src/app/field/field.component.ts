import {Component, ElementRef, OnInit, ViewChild, HostListener} from '@angular/core';
import {Gamedata} from '../gamedata.module';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})

export class FieldComponent implements OnInit {

  private gameOver = false;
  private showWindow = false;
  private restart = false;
  private pause = true;
  private printPause = true;
  private readonly EMPTY_CELL: string;
  private readonly SIZE_CELL: number;
  private readonly FIELD_WIDTH: number;
  private readonly FIELD_HEIGHT: number;
  private readonly field: string[][];
  private bufferFigure: { color: string, is_rotate: boolean, data: number[][] };
  private focusFigure: { color: string, is_rotate: boolean, data: number[][] };
  private position: { x: number, y: number };
  private info: { level: number, lines: number, score: number, speedLevel: number };
  private windowLevel: number;
  private windowScore: number;
  private level: number;
  private lines: number;
  private score: number;
  private speedLevel: number;

  @ViewChild('Buffer_field', {read: ElementRef}) Buffer_field: ElementRef;
  @ViewChild('Field') Field: ElementRef;
  private ctx: CanvasRenderingContext2D;
  private ctxBuffer: CanvasRenderingContext2D;

  constructor() {
    this.level = 1;
    this.lines = 0;
    this.score = 0;
    this.speedLevel = 900;
    this.EMPTY_CELL = Gamedata.EMPTY_CELL;
    this.SIZE_CELL = Gamedata.SIZE_CELL;
    this.FIELD_WIDTH = Gamedata.FIELD_WIDTH * this.SIZE_CELL;
    this.FIELD_HEIGHT = Gamedata.FIELD_HEIGHT * this.SIZE_CELL;
    this.field = this.initEmptyField(Gamedata.FIELD_WIDTH, Gamedata.FIELD_HEIGHT);
  }

  onPause() {
    this.keyPress({keyCode: 32});
  }

  onRestart() {
    this.restart = true;
    this.keyPress({keyCode: 32});
  }

  onHideWindow() {
    this.gameOver = false;
    this.showWindow = false;
  }

  ngOnInit() {
    const canvas: HTMLCanvasElement = this.Field.nativeElement;
    canvas.width = this.FIELD_WIDTH + 1;
    canvas.height = this.FIELD_HEIGHT + 1;
    this.ctx = canvas.getContext('2d');
    const canvasBuffer: HTMLCanvasElement = this.Buffer_field.nativeElement;
    canvasBuffer.width = 4 * this.SIZE_CELL + 1;
    canvasBuffer.height = 4 * this.SIZE_CELL + 1;
    this.ctxBuffer = canvasBuffer.getContext('2d');
    this.clearAll();

    const timer = () => {
      this.moveDown();
      setTimeout(timer, this.speedLevel);
    };
    setTimeout(timer, this.speedLevel);
  }

  initEmptyField(width, height) {
    const field = [];
    const temp = [];
    for (let i = 0; i < width; i++) {
      temp.push(this.EMPTY_CELL);
    }
    for (let j = 0; j < height; j++) {
      field.push(temp.slice());
    }
    return field;
  }


  draw(position, focusFigure, color, ctx) {
    for (let i = 0; i < focusFigure.data.length; i++) {
      ctx.fillStyle = color;
      ctx.fillRect(
        position.x * this.SIZE_CELL + focusFigure.data[i][0] * this.SIZE_CELL + 1,
        position.y * this.SIZE_CELL - focusFigure.data[i][1] * this.SIZE_CELL + 1,
        this.SIZE_CELL - 1,
        this.SIZE_CELL - 1
      );
    }
  }

  drawGrid(width, height, ctx) {
    ctx.fillStyle = 'white';
    ctx.fillRect(0.5, 0.5, width, height);
    ctx.strokeStyle = 'Black';
    ctx.strokeRect(0.5, 0.5, width, height);
    ctx.strokeStyle = '#DCDCDC';
    for (let i = 1; i < width / this.SIZE_CELL; i++) {
      ctx.moveTo(i * this.SIZE_CELL + 0.5, 1);
      ctx.lineTo(i * this.SIZE_CELL + 0.5, height);
    }
    for (let i = 1; i < height / this.SIZE_CELL; i++) {
      ctx.moveTo(1, i * this.SIZE_CELL + 0.5);
      ctx.lineTo(width, i * this.SIZE_CELL + 0.5);
    }
    ctx.stroke();
  }

  drawField(field, ctx) {
    for (let i = 0; i < field.length; i++) {
      for (let j = 0; j < field[i].length; j++) {
        ctx.fillStyle = field[i][j];
        ctx.fillRect(
          j * this.SIZE_CELL + 1,
          i * this.SIZE_CELL + 1,
          this.SIZE_CELL - 1,
          this.SIZE_CELL - 1
        );
      }
    }
  }

  drawPrintPause(ctx, ctxBuffer) {
    ctx.fillStyle = 'rgba(256, 256, 256, 0.8)';
    ctxBuffer.fillStyle = 'rgba(256, 256, 256, 0.8)';
    ctx.fillRect(1, 1, this.FIELD_WIDTH - 1, this.FIELD_HEIGHT - 1);
    ctxBuffer.fillRect(1, 1, 4 * this.SIZE_CELL - 1, 4 * this.SIZE_CELL - 1);
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillStyle = '#00F';
    ctx.strokeStyle = '#F00';
    ctx.font = 'italic 20pt Arial';
    ctx.fillText('PAUSE', this.FIELD_WIDTH / 2, this.FIELD_HEIGHT / 2);
    ctx.font = 'italic 10pt Arial';
    ctx.fillText('Press SPACE to continue..', this.FIELD_WIDTH / 2, this.FIELD_HEIGHT / 2 + 25);
  }

  clearAll() {
    for (let i = 0; i < this.field.length; i++) {
      for (let j = 0; j < this.field[i].length; j++) {
        this.field[i][j] = this.EMPTY_CELL;
      }
    }
    this.level = 1;
    this.lines = 0;
    this.score = 0;
    this.speedLevel = 900;
    this.pause = true;
    this.printPause = true;
    this.drawGrid(this.FIELD_WIDTH, this.FIELD_HEIGHT, this.ctx);
    this.drawGrid(4 * this.SIZE_CELL, 4 * this.SIZE_CELL, this.ctxBuffer);
    this.bufferFigure = Gamedata.getBufferFigure();
    this.focusFigure = Gamedata.getNextFigure(this.bufferFigure);
    this.bufferFigure = Gamedata.getBufferFigure();
    const minMax = Gamedata.searchMinMax(this.bufferFigure);
    this.draw({x: 0 - minMax.minX, y: minMax.maxY}, this.bufferFigure, this.bufferFigure.color, this.ctxBuffer);
    this.position = Gamedata.getStartPosition(this.focusFigure);
    this.draw(this.position, this.focusFigure, this.focusFigure.color, this.ctx);
  }

  checkDrawPause() {
    if (this.pause) {
      if (this.printPause) {
        this.drawPrintPause(this.ctx, this.ctxBuffer);
        this.printPause = false;
      }
      return;
    } else if (!this.printPause) {
      this.drawGrid(this.FIELD_WIDTH, this.FIELD_HEIGHT, this.ctx);
      this.drawGrid(4 * this.SIZE_CELL, 4 * this.SIZE_CELL, this.ctxBuffer);
      this.drawField(this.field, this.ctx);
      const minMax = Gamedata.searchMinMax(this.bufferFigure);
      this.draw({x: 0 - minMax.minX, y: minMax.maxY}, this.bufferFigure, this.bufferFigure.color, this.ctxBuffer);
      this.draw(this.position, this.focusFigure, this.focusFigure.color, this.ctx);
      this.printPause = true;
    }
  }

  moveDown() {
    this.checkDrawPause();
    if (this.pause || this.gameOver) {
      return;
    }
    if (Gamedata.checkNextLine(this.position, this.focusFigure, this.field)) {
      Gamedata.fillField(this.position, this.focusFigure, this.field);
      this.info = Gamedata.checkFullLine(this.field, this.level, this.lines, this.score, this.speedLevel);
      this.level = this.info.level;
      this.lines = this.info.lines;
      this.score = this.info.score;
      this.speedLevel = this.info.speedLevel;
      this.drawField(this.field, this.ctx);
      let minMax = Gamedata.searchMinMax(this.bufferFigure);
      this.draw({x: 0 - minMax.minX, y: minMax.maxY}, this.bufferFigure, this.EMPTY_CELL, this.ctxBuffer);
      this.focusFigure = Gamedata.getNextFigure(this.bufferFigure);
      this.bufferFigure = Gamedata.getBufferFigure();
      minMax = Gamedata.searchMinMax(this.bufferFigure);
      this.draw({x: 0 - minMax.minX, y: minMax.maxY}, this.bufferFigure, this.bufferFigure.color, this.ctxBuffer);
      this.position = Gamedata.getStartPosition(this.focusFigure);
      this.position.y--;
      if (Gamedata.checkNextLine(this.position, this.focusFigure, this.field)) {
        this.windowLevel = this.level;
        this.windowScore = this.score;
        this.showWindow = true;
        this.gameOver = true;
        this.clearAll();
        return;
      }
      this.position.y++;
      this.draw(this.position, this.focusFigure, this.focusFigure.color, this.ctx);
      return;
    }
    this.draw(this.position, this.focusFigure, this.EMPTY_CELL, this.ctx);
    this.position.y++;
    this.draw(this.position, this.focusFigure, this.focusFigure.color, this.ctx);
  }

  @HostListener('document:keydown', ['$event'])
  keyPress(e) {
    if ((this.pause && e.keyCode !== 32) || this.gameOver) {
      return;
    }
    if (e.keyCode === 32) {
      if (this.restart) {
        this.clearAll();
        this.restart = false;
        this.pause = false;
      }
      this.pause = !this.pause;
      this.checkDrawPause();
      return;
    }
    if (e.keyCode === 38) {
      if (!Gamedata.checkReverse(this.position, this.focusFigure, this.field)) {
        return;
      }
      this.draw(this.position, this.focusFigure, this.EMPTY_CELL, this.ctx);
      Gamedata.reverseFigure(this.focusFigure);
    }
    if (e.keyCode === 37) {
      if (!Gamedata.checkMove(this.position, this.focusFigure, this.field, -1)) {
        return;
      }
      this.draw(this.position, this.focusFigure, this.EMPTY_CELL, this.ctx);
      this.position.x--;
    }
    if (e.keyCode === 39) {
      if (!Gamedata.checkMove(this.position, this.focusFigure, this.field, 1)) {
        return;
      }
      this.draw(this.position, this.focusFigure, this.EMPTY_CELL, this.ctx);
      this.position.x++;
    }
    if (e.keyCode === 40) {
      this.score++;
      this.moveDown();
    }
    this.draw(this.position, this.focusFigure, this.focusFigure.color, this.ctx);
  }
}
